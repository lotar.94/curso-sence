/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package museo;

/**
 *
 * @author pc
 */
public class Sala {
    
    private String nombre;
    private int numeroDeLamparas;
    private double temperaturaDeSala;
    private boolean cierreCentralizado;
    private boolean alarmaDeIncendios;

    public Sala(String nombre, int numeroDeLamparas, double temperaturaDeSala, boolean cierreCentralizado, boolean alarmaDeIncendios) {
        this.nombre = nombre;
        this.numeroDeLamparas = numeroDeLamparas;
        this.temperaturaDeSala = temperaturaDeSala;
        this.cierreCentralizado = cierreCentralizado;
        this.alarmaDeIncendios = alarmaDeIncendios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroDeLamparas() {
        return numeroDeLamparas;
    }

    public void setNumeroDeLamparas(int numeroDeLamparas) {
        this.numeroDeLamparas = numeroDeLamparas;
    }

    public double getTemperaturaDeSala() {
        return temperaturaDeSala;
    }

    public void setTemperaturaDeSala(double temperaturaDeSala) {
        this.temperaturaDeSala = temperaturaDeSala;
    }

    public boolean isCierreCentralizado() {
        return cierreCentralizado;
    }

    public void setCierreCentralizado(boolean cierreCentralizado) {
        this.cierreCentralizado = cierreCentralizado;
    }

    public boolean isAlarmaDeIncendios() {
        return alarmaDeIncendios;
    }

    public void setAlarmaDeIncendios(boolean alarmaDeIncendios) {
        this.alarmaDeIncendios = alarmaDeIncendios;
    }
    
    
    
}
