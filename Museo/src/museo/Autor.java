/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package museo;

/**
 *
 * @author pc
 */
public class Autor extends Persona{

    private String nacionalidad;

    public Autor(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }


   

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
    
}
