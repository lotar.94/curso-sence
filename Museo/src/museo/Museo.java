/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package museo;

import java.util.Scanner;

/**
 *
 * @author pc
 */
public class Museo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner leer = new Scanner(System.in);
        
        Sala sala1 = new Sala("Da Vinci", 10, 22.3, true, false);

        System.out.println("Ingresa el rut del autor.");
        
        Validacion validaRut = new Validacion();
        
        String rut = leer.nextLine();
        boolean resultado;    
        resultado = validaRut.validarRut(rut);
        
        if(resultado){
            Autor DaVinci = new Autor("Chile");
        
            Tamano tamano = new Tamano(100, 150);

            ObraDeArte monaLisa = new ObraDeArte(DaVinci, 1850, "Mona Lisa", tamano, sala1, 001);

//            System.out.println("El autor es: "+DaVinci.getNombre()
//                    +"\nLa obra es: "+monaLisa.getTitulo()
//                    +"\nEl rut del autor es: "+DaVinci.getRut()
//                    +"\nLa fecha de creacion es:"+monaLisa.getAnioCreacion()
//                    +"\nEsta en la sala: "+sala1.getNombre()
//            );
        }else{
            System.out.println("El RUT no es valido");
        }
        
        
    }
    
}
