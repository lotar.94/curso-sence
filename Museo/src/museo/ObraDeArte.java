/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package museo;

/**
 *
 * @author pc
 */
public class ObraDeArte {
    
    private Autor autor;
    private int anioCreacion;
    private String titulo;
    private Tamano tamano;
    private Sala ubicacion;
    private int idObra;

    public ObraDeArte(Autor autor, int anioCreacion, String titulo, Tamano tamano, Sala ubicacion, int idObra) {
        this.autor = autor;
        this.anioCreacion = anioCreacion;
        this.titulo = titulo;
        this.tamano = tamano;
        this.ubicacion = ubicacion;
        this.idObra = idObra;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public int getAnioCreacion() {
        return anioCreacion;
    }

    public void setAnioCreacion(int anioCreacion) {
        this.anioCreacion = anioCreacion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Tamano getTamano() {
        return tamano;
    }

    public void setTamano(Tamano tamano) {
        this.tamano = tamano;
    }

    public Sala getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Sala ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getIdObra() {
        return idObra;
    }

    public void setIdObra(int idObra) {
        this.idObra = idObra;
    }
    
    
}
