/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio12listas;

/**
 *
 * @author pc
 */
public class Persona {
    
    private String nombre;
    private String apellido;
    private int agnoNacimiento;

    public Persona(String nombre, String apellido, int agnoNacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.agnoNacimiento = agnoNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getAgnoNacimiento() {
        return agnoNacimiento;
    }

    public void setAgnoNacimiento(int agnoNacimiento) {
        this.agnoNacimiento = agnoNacimiento;
    }
    
    
    
    
    
}
