/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio12listas;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author pc
 */
public class Ejercicio12Listas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        // TODO code application logic here
        
        
        Persona p1=new Persona("juan", "Gonzalez", 1990);
        Persona p2=new Persona("Francisco", "Perez", 1888);
        
        ArrayList<Persona> listaPersonas = new ArrayList<>();
    
        listaPersonas.add(p1);
        listaPersonas.add(0,p2);
        
        //este codigo obtiene una persona que esta en la posicion 00 y utiliza los metodos que tiene ese objeto
        System.out.println("Persona1 nombre: "+listaPersonas.get(0).getNombre());
        System.out.println("Persona1 apellido: "+listaPersonas.get(0).getApellido());
        
        
        
        Iterator <Persona>iterador = listaPersonas.iterator();
        
        System.out.println("Esto recorre la lista");
        
        
        while(iterador.hasNext()){
            Persona persona = iterador.next();
            System.out.println("Nombre: "+persona.getNombre());
            System.out.println("Apellido: "+persona.getApellido());
            System.out.println("Fecha Nacimiento: "+persona.getAgnoNacimiento());
        }
        
    }    
    
}
