/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;
  
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author pc
 */
public class EjercicioLista {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        ArrayList <String> lista;
        
        lista = new ArrayList<>();
        
        //.add esto agrega elementos a la lista
        lista.add("elemento1");
        lista.add("elemento2");
        lista.add("elemento2");
        lista.add("elemento3");
        //.add con dos parameros agregar un elemento en un posicion especifica
        lista.add(2, "elemento4");
        lista.add("elemento2");
        lista.add(0, "elemento5");
        //.remove elimina un objeto de la lista
        lista.remove("elemento3");
        
        //.get obtiene el objeto de la lista con el indice indicado en el parametro indicado
        System.out.println(lista.get(0));
        System.out.println(lista.get(2));
        
        //.contains verifica si el objeto indicado esta en la lista y devuelve un true o false
        System.out.println(lista.contains("elemento3"));
        
        //.size esto devuelve el tamaño de la lista
        System.out.println("El largo de la lista_ "+lista.size());
        
        //.lastIndexOf esto encuentra el indice del ultimo elemento indicado 
        System.out.println("el ultimo objeto de la lista elemento2: "+lista.lastIndexOf("elemento2"));
        
        //.isEmpty esto devuelve un true o false si la lista esta vacia
        System.out.println("es vacia?"+lista.isEmpty());
        
        //..(ArrayList<String>) esto transforma hace un casting de lista y lo transforma en una array de string
        ArrayList <String> lista2 = (ArrayList<String>)lista.clone(); 
                
        
    }
    
}
