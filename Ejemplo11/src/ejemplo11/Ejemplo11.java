/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo11;

/**
 *
 * @author pc
 */
public class Ejemplo11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Futbolista futbolista1;
        
        Menu menu = new Menu();
        
        futbolista1 =  menu.solicitaDatos();
        
        System.out.println("Nombre futbolista: "+futbolista1.getNombre()+"\n"+
                            "El dorso del jugador es: "+futbolista1.getDorsal()+"\n"+
                            "La posicion del jugador es: "+futbolista1.getDemarcacion()+"\n"+
                            "El equipo del jugador es: "+futbolista1.getEquipo());
        
    }
    
}
